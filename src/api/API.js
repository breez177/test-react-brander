import axios from "axios";

export class API {
  apiUrl = process.env.REACT_APP_API_URL;

  getM (endpoint) {
    const self = this
    return new Promise(function (resolve, reject) {
      axios.get(`${self.apiUrl}${endpoint}`).then(function (resp) {
        resolve(resp.data)
      }).catch(function (err) {
        self.handleError(err)
        reject(err.response)
      })
    })
  }

  handleError (err) {
    console.error(err)
  }
}
