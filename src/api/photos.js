import { API } from './API'

export class PhotosList {
  Api;
  endpoint = 'photos';

  constructor () {
    this.Api = new API()
  }

  getPhotos (searchTerm) {
    return this.Api.getM(`${this.endpoint}?title_like=${searchTerm}`);
  }
}
