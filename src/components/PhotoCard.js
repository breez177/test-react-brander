const PhotoCard = ({ photo }) => {
  return (
    <div className={'ph-card'}>
      <p className={'ph-card_name'}>
        { photo.title }
      </p>
      <div className={'ph-card_picture'}>
        <img
          className={'ph-card_picture-item'}
          src={ photo.url }
          alt={ photo.title }
        />
      </div>
    </div>
  );
};

export default PhotoCard;
