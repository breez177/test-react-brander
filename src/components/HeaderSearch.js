import React, { useState, useEffect } from "react";

const HeaderSearch = ({ searchTerm, onSearchChange }) => {
  const [inputValue, setInputValue] = useState(searchTerm);
  const [typingTimeout, setTypingTimeout] = useState(null);

  const handleSearch = (event) => {
    const value = event.target.value;
    setInputValue(value);

    clearTimeout(typingTimeout);

    const timeout = setTimeout(() => {
      onSearchChange(value);
    }, 300);

    setTypingTimeout(timeout);
  };

  useEffect(() => {
    return () => {
      clearTimeout(typingTimeout);
    };
  }, [typingTimeout]);

  return (
    <div className={'hd-search'}>
      <a href='#' className={'hd-search_logo'}>Ph</a>
      <div className={'hd-search_search'}>
        <input
          type="text"
          placeholder={'Search...'}
          value={inputValue}
          onChange={handleSearch}
          className={'hd-search_search-text-field'}
        />
      </div>
    </div>
  );
};

export default HeaderSearch;
