import React, { useState, useEffect, useCallback } from "react";
import HeaderSearch from "./components/HeaderSearch";
import { PhotosList } from './api/photos';
import PhotoCard from "./components/PhotoCard";

function App() {
  const [searchTerm, setSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [loading, setLoading] = useState(false);

  const fetchSearchResults = useCallback(async (term) => {
    setLoading(true);
    const results = await (new PhotosList()).getPhotos(term);
    setSearchResults(results);
    setLoading(false);
  }, []);

  useEffect(() => {

    const searchPhotos = async () => {
      if (!searchTerm.length) {
        setSearchResults([]);
        return;
      }

      setLoading(true);

      const delayDebounceFn = setTimeout(async () => {
        await fetchSearchResults(searchTerm);
        setLoading(false);
      }, 300);

      return () => {
        clearTimeout(delayDebounceFn);
      };
    };

    searchPhotos();
  }, [searchTerm, fetchSearchResults]);

  const handleSearchChange = useCallback((value) => {
    setSearchTerm(value);
  }, []);

  const renderPhotos = () => {
    return searchResults.map(photo => (
        <PhotoCard key={photo.id} photo={photo} />
    ));
  };

  return (
    <>
      <HeaderSearch
        searchTerm={searchTerm}
        onSearchChange={handleSearchChange}
      />
      <div className="container">
        <div className={'phgr-search'}>
          {loading ? (
            <p>Loading...</p>
          ) : !searchResults.length ? (
            <p>Nothing to show</p>
          ) : (
            renderPhotos()
          )}
        </div>
      </div>
    </>
  );
}

export default App;
